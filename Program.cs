﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Azure.EventHubs;
using Newtonsoft.Json;

namespace LocalGenerator
{
    internal class Program
    {
        private const int numberOfFirstMeter = 10001;
        private const int numberOfMeters = 100;
        private static Random rand = new Random();

        public static async Task<List<string>> GetMeterNames()
        {
            var meters = new List<string>();

            try
            {
                using (var conn = new SqlConnection("Server=tcp:poe-test500.database.windows.net,1433;Initial Catalog=poe;Persist Security Info=False;User ID=poe-test;Password=Vecp0JK91Zj*;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;"))
                {
                    conn.Open();
                    using (var cmd = new SqlCommand())
                    {
                        cmd.Connection = conn;
                        cmd.CommandText =
                            "select SystemName from dbo.Meters where Active = 1";
                        using (var reader = await cmd.ExecuteReaderAsync())
                        {
                            while (reader.Read())
                            {
                                meters.Add(reader[0].ToString());
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

            return meters;
        }

        public static async Task<List<MeterTzPoco>> GetMetersWithTz()
        {
            var meters = new List<MeterTzPoco>();

            try
            {
                using (var conn = new SqlConnection("Server=tcp:poe-test500.database.windows.net,1433;Initial Catalog=poe;Persist Security Info=False;User ID=poe-test;Password=Vecp0JK91Zj*;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;"))
                {
                    conn.Open();
                    using (var cmd = new SqlCommand())
                    {
                        cmd.Connection = conn;
                        cmd.CommandText =
                            "select Id, SystemName, TimeZone from dbo.Meters where Active = 1";
                        using (var reader = await cmd.ExecuteReaderAsync())
                        {
                            while (reader.Read())
                            {
                                meters.Add(new MeterTzPoco { Id = int.Parse(reader[0].ToString()), SystemName = reader[1].ToString(), TimeZone = reader[2].ToString() });
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return meters;
        }

        public static IDictionary<string, string> GetMeterTimezone(IEnumerable<string> meters)
        {
            var result = new Dictionary<string, string>();

            try
            {
                using (var conn = new SqlConnection("Server=tcp:poe-test500.database.windows.net,1433;Initial Catalog=poe;Persist Security Info=False;User ID=poe-test;Password=Vecp0JK91Zj*;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;"))
                {
                    conn.Open();
                    var command = conn.CreateCommand();
                    command.CommandText = "select TimeZone from Meters where SystemName like @SystemName";

                    foreach (var meter in meters)
                    {
                        command.Parameters.Clear();
                        command.Parameters.Add("@SystemName", SqlDbType.VarChar).Value = meter;
                        var timeZone = command.ExecuteScalar();
                        result.Add(meter, timeZone is DBNull ? string.Empty : timeZone.ToString());
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine($"GetNames exception: {e}");
            }

            return result;
        }

        private static void AddMeter(string name)
        {
            try
            {
                using (var cnn = new SqlConnection("Server=tcp:poe-test500.database.windows.net,1433;Initial Catalog=poe;Persist Security Info=False;User ID=poe-test;Password=Vecp0JK91Zj*;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;"))
                {
                    cnn.Open();
                    using (var cmd = cnn.CreateCommand())
                    {
                        cmd.CommandText = $"INSERT INTO dbo.Meters VALUES(@name, @systemName, @desc, @organizationId, @tz, @fv, @ip, @serial, @type, @gsm, @sim, @loc, @tariff, @power, @area, @amqp, @active)";
                        cmd.Parameters.AddWithValue("name", name);
                        cmd.Parameters.AddWithValue("systemName", name);
                        cmd.Parameters.AddWithValue("desc", name);
                        cmd.Parameters.AddWithValue("organizationId", 88);
                        cmd.Parameters.AddWithValue("tz", "Central European Standard Time");
                        cmd.Parameters.AddWithValue("fv", "-");
                        cmd.Parameters.AddWithValue("ip", "ip");
                        cmd.Parameters.AddWithValue("serial", "-");
                        cmd.Parameters.AddWithValue("type", "-");
                        cmd.Parameters.AddWithValue("gsm", false);
                        cmd.Parameters.AddWithValue("sim", "-");
                        cmd.Parameters.AddWithValue("loc", "-");
                        cmd.Parameters.AddWithValue("tariff", 228);
                        cmd.Parameters.AddWithValue("power", "10000");
                        cmd.Parameters.AddWithValue("area", "10000");
                        cmd.Parameters.AddWithValue("amqp", "-");
                        cmd.Parameters.AddWithValue("active", true);
                        cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception: " + ex);
            }
        }

        private static string GenerateValues()
        {
            return $"U1:" + rand.Next(200, 250) + "&" +
                    "U2:" + rand.Next(200, 250) + "&" +
                    "U3:" + rand.Next(200, 250) + "&" +
                    "I1:" + rand.Next(5000, 20000) + "&" +
                    "I2:" + rand.Next(5000, 20000) + "&" +
                    "I3:" + rand.Next(5000, 20000) + "&" +
                    "In:" + rand.Next(5000, 20000) + "&" +
                    "PT:" + rand.Next(5000, 20000) + "&" +
                    "QT:" + rand.Next(5000, 20000) + "&" +
                    "PF:" + rand.Next(1, 10) + "&" +
                    "P1:" + rand.Next(5000, 20000) + "&" +
                    "P2:" + rand.Next(5000, 20000) + "&" +
                    "P3:" + rand.Next(5000, 20000) + "&" +
                    "Q1:" + rand.Next(5000, 20000) + "&" +
                    "Q2:" + rand.Next(5000, 20000) + "&" +
                    "Q3:" + rand.Next(5000, 20000) + "&" +
                    "A40_COS_L1:" + rand.NextDouble() + "&" +
                    "A40_COS_L2:" + rand.NextDouble() + "&" +
                    "A40_COS_L3:" + rand.NextDouble() + "&" +
                    "A40_TG_L1:" + rand.NextDouble() + "&" +
                    "A40_TG_L2:" + rand.NextDouble() + "&" +
                    "A40_TG_L3:" + rand.NextDouble() + "&" +
                    "Ea_PLUS:" + rand.Next(1, 100) + "&" +
                    "U_Har3_L1:" + rand.Next(1, 100) + "&" +
                    "U_Har3_L2:" + rand.Next(1, 100) + "&" +
                    "U_Har3_L3:" + rand.Next(1, 100) + "&" +
                    "U_Har5_L1:" + rand.Next(1, 100) + "&" +
                    "U_Har5_L2:" + rand.Next(1, 100) + "&" +
                    "U_Har5_L3:" + rand.Next(1, 100) + "&" +
                    "U_Har7_L1:" + rand.Next(1, 100) + "&" +
                    "U_Har7_L2:" + rand.Next(1, 100) + "&" +
                    "U_Har7_L3:" + rand.Next(1, 100) + "&" +
                    "U_Har9_L1:" + rand.Next(1, 100) + "&" +
                    "U_Har9_L2:" + rand.Next(1, 100) + "&" +
                    "U_Har9_L3:" + rand.Next(1, 100) + "&" +
                    "U_Har11_L1:" + rand.Next(1, 100) + "&" +
                    "U_Har11_L2:" + rand.Next(1, 100) + "&" +
                    "U_Har11_L3:" + rand.Next(1, 100) + "&" +
                    "U_Har13_L1:" + rand.Next(1, 100) + "&" +
                    "U_Har13_L2:" + rand.Next(1, 100) + "&" +
                    "U_Har13_L3:" + rand.Next(1, 100) + "&" +
                    "U_Har15_L1:" + rand.Next(1, 100) + "&" +
                    "U_Har15_L2:" + rand.Next(1, 100) + "&" +
                    "U_Har15_L3:" + rand.Next(1, 100) + "&" +
                    "I_Har3_L1:" + rand.Next(1000, 10000) + "&" +
                    "I_Har3_L2:" + rand.Next(1000, 10000) + "&" +
                    "I_Har3_L3:" + rand.Next(1000, 10000) + "&" +
                    "I_Har5_L1:" + rand.Next(1000, 10000) + "&" +
                    "I_Har5_L2:" + rand.Next(1000, 10000) + "&" +
                    "I_Har5_L3:" + rand.Next(1000, 10000) + "&" +
                    "I_Har7_L1:" + rand.Next(1000, 10000) + "&" +
                    "I_Har7_L2:" + rand.Next(1000, 10000) + "&" +
                    "I_Har7_L3:" + rand.Next(1000, 10000) + "&" +
                    "I_Har9_L1:" + rand.Next(1000, 10000) + "&" +
                    "I_Har9_L2:" + rand.Next(1000, 10000) + "&" +
                    "I_Har9_L3:" + rand.Next(1000, 10000) + "&" +
                    "I_Har11_L1:" + rand.Next(1000, 10000) + "&" +
                    "I_Har11_L2:" + rand.Next(1000, 10000) + "&" +
                    "I_Har11_L3:" + rand.Next(1000, 10000) + "&" +
                    "I_Har13_L1:" + rand.Next(1000, 10000) + "&" +
                    "I_Har13_L2:" + rand.Next(1000, 10000) + "&" +
                    "I_Har13_L3:" + rand.Next(1000, 10000) + "&" +
                    "I_Har15_L1:" + rand.Next(1000, 10000) + "&" +
                    "I_Har15_L2:" + rand.Next(1000, 10000) + "&" +
                    "I_Har15_L3:" + rand.Next(1000, 10000) + "&" +
                    "In_Har3:" + rand.Next(1000, 10000) + "&" +
                    "In_Har5:" + rand.Next(1000, 10000) + "&" +
                    "In_Har7:" + rand.Next(1000, 10000) + "&" +
                    "In_Har9:" + rand.Next(1000, 10000) + "&" +
                    "In_Har11:" + rand.Next(1000, 10000) + "&" +
                    "In_Har13:" + rand.Next(1000, 10000) + "&" +
                    "In_Har15:" + rand.Next(1000, 10000) + "&" +
                    "THD_U1:" + rand.NextDouble() + "&" +
                    "THD_U2:" + rand.NextDouble() + "&" +
                    "THD_U3:" + rand.NextDouble() + "&" +
                    "THD_I1:" + rand.Next(10, 100) + "&" +
                    "THD_I2:" + rand.Next(10, 100) + "&" +
                    "THD_I3:" + rand.Next(10, 100) + "&" +
                    "THD_In:" + rand.Next(10, 100) + "&" +
                    "QT_lag:" + rand.Next(10, 100) + "&" +
                    "QT_lead:" + rand.Next(100, 1000) + "&" +
                    "Er_lag_dec:" + 0 + "&" +
                    "Er_lag_res:" + 0 + "&" +
                    "Er_lead_dec:" + 0 + "&" +
                    "Er_lead_res:" + 0;
        }

        private static void Main(string[] args)
        {
            //for (int i = 1; i <= 20; i++)
            //{
            //    for (int j = 1; j <= 80; j++)
            //    {
            //        AddMeter("test" + i + "_" + j);
            //    }
            //}

            //var meters = GetMetersWithTz().GetAwaiter().GetResult();
            //var tz = GetMeterTimezone(names);

            //MetersToConnection();

            Task.Factory.StartNew(() => ProcessSend2("1"));
            Task.Factory.StartNew(() => ProcessSend2("2"));
            Task.Factory.StartNew(() => ProcessSend2("3"));
            Task.Factory.StartNew(() => ProcessSend2("4"));
            Task.Factory.StartNew(() => ProcessSend2("5"));
            Task.Factory.StartNew(() => ProcessSend2("6"));
            Task.Factory.StartNew(() => ProcessSend2("7"));
            Task.Factory.StartNew(() => ProcessSend2("8"));
            Task.Factory.StartNew(() => ProcessSend2("9"));
            Task.Factory.StartNew(() => ProcessSend2("10"));
            Task.Factory.StartNew(() => ProcessSend2("11"));
            Task.Factory.StartNew(() => ProcessSend2("12"));
            Task.Factory.StartNew(() => ProcessSend2("13"));
            Task.Factory.StartNew(() => ProcessSend2("14"));
            Task.Factory.StartNew(() => ProcessSend2("15"));
            Task.Factory.StartNew(() => ProcessSend2("16"));
            Task.Factory.StartNew(() => ProcessSend2("17"));
            Task.Factory.StartNew(() => ProcessSend2("18"));
            Task.Factory.StartNew(() => ProcessSend2("19"));
            Task.Factory.StartNew(() => ProcessSend2("20"));

            Console.ReadKey();
        }

        private static void MetersToConnection()
        {
            try
            {
                var meters = GetMetersWithTz().GetAwaiter().GetResult();

                using (var cnn = new SqlConnection("Server=tcp:poe-test500.database.windows.net,1433;Initial Catalog=poe;Persist Security Info=False;User ID=poe-test;Password=Vecp0JK91Zj*;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;"))
                {
                    cnn.Open();
                    using (var cmd = cnn.CreateCommand())
                    {
                        cmd.CommandText = $"INSERT INTO dbo.ConnectionsMetersLink VALUES(@ConnectionId, @MeterId)";

                        meters.ForEach(m =>
                        {
                            cmd.Parameters.Clear();
                            cmd.Parameters.AddWithValue("ConnectionId", 15);
                            cmd.Parameters.AddWithValue("MeterId", m.Id);
                            cmd.ExecuteNonQuery();
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception: " + ex);
            }
        }

        private async static void ProcessSend2(string s)
        {
            var connectionStringBuilder = new EventHubsConnectionStringBuilder("Endpoint=sb://poe-test500.servicebus.windows.net/;SharedAccessKeyName=RootManageSharedAccessKey;SharedAccessKey=pjx7zurznwWgN0DKrMulGt5aH1M/ytpsoTlUOH/h9HM=")
            {
                EntityPath = "ipoe-data"
            };

            var eventHubClient = EventHubClient.CreateFromConnectionString(connectionStringBuilder.ToString());
            var sw = new Stopwatch();
            var eventList = new List<EventData>();

            while (true)
            {
                sw.Restart();
                string redisLog = "";

                // Create a batch of events
                //CreateBatchOptions o = new CreateBatchOptions();
                //o.PartitionKey = p.ToString();
                //EventDataBatch eventBatch = await producerClient.CreateBatchAsync();

                var date = DateTime.UtcNow;
                date = date.AddHours(2);
                var frame = new RawPoco();
                frame.Key = "all_var";
                frame.Eventtime = date.Year + "-" +
                                  ((date.Month > 9) ? date.Month.ToString() : ("0" + date.Month)) +
                                  "-" + ((date.Day > 9) ? date.Day.ToString() : ("0" + date.Day)) + "T" +
                                  ((date.Hour > 9) ? date.Hour.ToString() : ("0" + date.Hour)) + ":" +
                                  ((date.Minute > 9) ? date.Minute.ToString() : ("0" + date.Minute)) + ":" +
                                  ((date.Second > 9) ? date.Second.ToString() : ("0" + date.Second));
                frame.Value = GenerateValues();

                eventList.Clear();
                //redisLog += "part1: " + sw.ElapsedMilliseconds;
                for (int i = 1; i <= 60; i++)
                {
                    frame.Collection = "test" + s + "_" + i;

                    string json = JsonConvert.SerializeObject(frame);
                    eventList.Add(new EventData(Encoding.UTF8.GetBytes(json)));
                }

                eventHubClient.SendAsync(eventList, s).Wait();
                //producerClient.SendAsync(eventBatch).Wait();

                //redisLog += " part2: " + sw.ElapsedMilliseconds;
                //redisLog += " part3: " + sw.ElapsedMilliseconds;
                //await RedisDatabase.StringSetAsync("performance_tester", redisLog);
                sw.Stop();
                Console.WriteLine($"Sent from {s}, time: {sw.ElapsedMilliseconds}, second: {date.Second}");

                Thread.Sleep(1000);
            }
        }
    }
}