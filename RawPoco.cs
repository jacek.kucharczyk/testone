﻿using System.Collections.Generic;

namespace LocalGenerator
{
    internal class RawPoco
    {
        public string Collection { get; set; }
        public string Eventtime { get; set; }
        public string Key { get; set; }
        public Dictionary<string, double> Points { get; set; }
        public string Raw { get; set; }
        public string Value { get; set; }
    }
}